<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('servers', function (Blueprint $table) {
            $table->dropColumn([ 'ip_address', 'path', 'start_script' ]);

            $table->string('max_memory')->nullable();
            $table->string('start_memory')->nullable();
            $table->string('jvm_arguments')->nullable();
            $table->string('server_jar')->nullable();

            $table->string('type');
            $table->boolean('created')->default(false);

            // Basic server properties to override
            $table->string('seed')->nullable();
            $table->string('world_name')->nullable();
            $table->string('motd')->nullable();
            $table->integer('difficulty')->nullable();
            $table->string('level_type')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('servers', function (Blueprint $table) {
            $table->dropColumn([
                'type',
                'created',
                'seed',
                'world_name',
                'motd',
                'difficulty',
                'level_type',
                'server_jar',
                'jvm_arguments',
                'max_memory',
                'start_memory',

            ]);

            $table->string('path');
            $table->string('start_script');
            $table->string('ip_address');
        });
    }
}
