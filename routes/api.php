<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//Route::group([ 'middleware' => 'auth:api' ], function () {
    Route::get('server/{server}/status', 'API\\ServerController@getStatus');
    Route::get('server/{server}/start', 'API\\ServerController@startServer');
    Route::get('server/{server}/waitStart', 'API\\ServerController@waitForStart');
    Route::get('server/{server}/stop', 'API\\ServerController@stopServer');
    Route::get('server/{server}/kill', 'API\\ServerController@killServer');
    Route::get('server/{server}/players', 'API\\ServerController@players');

    Route::post('server/{server}/sendCommand', 'API\\ServerController@sendCommand');

    Route::post('server/store', 'API\\ServerController@store');
    Route::delete('server/{server}', 'API\\ServerController@destroy');
    Route::put('server/{server}/update', 'API\\ServerController@update');

    Route::get('server/{server}/getModsDownloadUrl', 'API\\ServerController@zipMods');
//});

