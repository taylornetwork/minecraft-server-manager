<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/server');
});

Auth::routes();

Route::get('/home', function () {
    return redirect('/server');
})->name('home');

Route::get('/server', 'ServerController@all')->name('server.all');
Route::get('/server/create', 'ServerController@create')->name('server.create');

//Route::get('/server/{server}/properties', 'ServerController@properties')->name('server.properties');
Route::get('/server/{server}/edit', 'ServerController@edit')->name('server.edit');
Route::get('/server/{server}/download', 'ServerController@download')->name('server.download');
