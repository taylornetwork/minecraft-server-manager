<?php

namespace App\Observers;

use App\Server;
use App\Support\Server\Creator;

class ServerObserver
{
    /**
     * Do create tasks on create
     *
     * @param Server $server
     */
    public function created(Server $server)
    {
        (new Creator($server))->create();
    }

    public function deleting(Server $server)
    {

    }
}