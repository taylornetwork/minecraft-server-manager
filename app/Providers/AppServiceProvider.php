<?php

namespace App\Providers;

use App\Observers\ServerObserver;
use App\Server;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Server::observe(ServerObserver::class);

        // Override LFM translations
        $this->loadTranslationsFrom(base_path('lang'), 'laravel-filemanager');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
