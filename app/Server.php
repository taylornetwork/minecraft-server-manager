<?php

namespace App;

use App\Support\Properties\Manager as PropertiesManager;
use App\Support\Server\Manager as ServerManager;
use App\Support\Shell\ShellCommand;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use ZipArchive;

class Server extends Model
{
    /**
     * @inheritDoc
     */
    protected $fillable = [
        'name', 'display_name', 'port', 'modded', 'download_path', 'type', 'created', 'max_memory', 'start_memory',
        'jvm_arguments', 'server_jar', 'seed', 'world_name', 'difficulty', 'level_type', 'motd', 'started_at',
    ];

    /**
     * Append to JSON
     *
     * @var array
     */
    protected $appends = [ 'address', 'properties' ];

    protected $dates = [ 'started_at' ];

    /**
     * Map of database fields to server.properties fields
     *
     * @var array
     */
    public $propertiesMap = [
        'seed' => 'level-seed',
        'world_name' => 'level-name',
        'difficulty' => 'difficulty',
        'level_type' => 'level-type',
        'motd' => 'motd',
        'port' => 'server-port',
    ];

    /**
     * Start the server
     *
     * @return ShellCommand
     */
    public function start()
    {
        $cmd = $this->getManager()->start();
        $this->update([ 'started_at' => Carbon::now() ]);
        return $cmd;
    }

    /**
     * Stop the server
     *
     * @return ShellCommand
     */
    public function stop()
    {
        $cmd = $this->getManager()->stop();
        $this->update([ 'started_at' => null ]);
        return $cmd;
    }

    /**
     * Kill the server
     *
     * @return ShellCommand
     */
    public function kill()
    {
        $cmd = $this->getManager()->forceStop();
        $this->update([ 'started_at' => null ]);
        return $cmd;
    }

    /**
     * Get server status
     *
     * @return string
     */
    public function status()
    {
        return $this->getManager()->status();
    }

    public function waitForStart()
    {
        if(!$this->getManager()->isOn()) {
            $this->start();
        }

        do {
           $done = $this->doneStarting();
           sleep(30);
        } while (!$done);

        return true;
    }

    /**
     * Get the number of players online right now
     *
     * @param array  $output
     * @return string
     */
    public function getPlayerCountFromOutput($output)
    {
        $patterns = [
            '/are (\d*)\/(\d*)/',
            '/(\d*) of a max (\d*)/',
        ];

        $found = [];

        foreach($patterns as $pattern) {
            foreach($output as $line) {
                if(preg_match($pattern, $line, $matches)) {
                    $found = $matches;
                }
            }
        }


        /*
         * $found = [
         *      ' #a/#b ',
         *      '#a',
         *      '#b',
         * ]
         */

        return $found[1];
    }

    /**
     * Get list of players from output
     *
     * @param array  $output
     * @return array
     */
    public function getPlayerListFromOutput($output)
    {
        // change this whole thing now
       if($this->getPlayerCountFromOutput($output) === '0') {
           return [];
       }

       $pattern = '#(\[.*?\])? ?:?#';
       $replaced = preg_replace($pattern, '', $output[3]); // @todo handles only a few - revisit

       return explode(',', $replaced);
    }

    /**
     * Get players online
     *
     * @return array
     */
    public function players()
    {
        $output = $this->send('list');

        return [
            'count' => $this->getPlayerCountFromOutput($output),
            'list' => $this->getPlayerListFromOutput($output),
        ];
    }

    /**
     * Send a command to server and get output
     *
     * @param string  $command
     * @return array
     */
    public function send($command)
    {
        return $this->getManager()->send($command);
    }

    /**
     * Send a command to server but don't get the output
     *
     * @param string  $command
     * @return ShellCommand|bool
     */
    public function sendNoOutput($command)
    {
        return $this->getManager()->sendCommand($command);
    }

    /**
     * Is the server on?
     *
     * @return bool
     */
    public function isOn()
    {
        return $this->getManager()->isOn();
    }

    /**
     * Get an instance of ServerManager
     *
     * @return ServerManager
     */
    public function getManager()
    {
        return new ServerManager($this);
    }

    /**
     * Get properties from server.properties
     *
     * @return array
     */
    public function getProperties()
    {
        return (new PropertiesManager($this))->properties;
    }

    /**
     * Get the server path
     *
     * @param string $subDir
     * @return string
     */
    public function getPath(string $subDir = null)
    {
        $basePath = config('server.basePath');
        if(last(explode(DIRECTORY_SEPARATOR, $basePath)) !== '') {
            $basePath .= DIRECTORY_SEPARATOR;
        }

        return $basePath . $this->name . (empty($subDir) ? '' : DIRECTORY_SEPARATOR . trim($subDir, DIRECTORY_SEPARATOR));
    }

    /**
     * Get the command to start
     *
     * @return string
     */
    public function getCommandAttribute()
    {
        $arguments = [];

        if($this->max_memory !== null) {
            $arguments[] = '-Xmx' . $this->max_memory;
        }

        if($this->start_memory !== null) {
            $arguments[] = '-Xms' . $this->start_memory;
        }

        if($this->jvm_arguments !== null) {
            $arguments[] = $this->jvm_arguments;
        }

        return 'java -jar ' . implode(' ', $arguments) . ' ' . $this->server_jar;
    }

    /**
     * Get the address of the server
     *
     * @return string
     */
    public function getAddressAttribute()
    {
        return preg_replace('#(f|ht)tps?://#', '', env('MC_URL', env('APP_URL')));
    }

    public function getPropertiesAttribute()
    {
        return $this->getProperties();
    }

    public function doneStarting()
    {
        $done = false;
        $output = $this->getManager()->getScreen(10000)->output;

        foreach($output as $line) {
            if(preg_match('~]: Done \(\d+\.\d+s\)!~', $line)) {
                $done = true;
                break;
            }
        }

        return $done;
    }

}
