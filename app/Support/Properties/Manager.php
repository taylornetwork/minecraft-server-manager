<?php

namespace App\Support\Properties;

use App\Server;

class Manager
{
    /**
     * @var Server
     */
    protected $server;

    /**
     * Server Properties
     *
     * @var array
     */
    protected $properties = [];

    /**
     * server.properties file path
     *
     * @var string
     */
    protected $propertiesFile;

    /**
     * PropertiesManager constructor.
     *
     * @param Server $server
     */
    public function __construct(Server $server)
    {
        $this->server = $server;
        $this->propertiesFile = $this->server->getPath() . DIRECTORY_SEPARATOR . 'server.properties';
        $this->getProperties();
    }

    /**
     * Get properties from file
     *
     * @return array
     */
    public function getProperties()
    {
        foreach(file($this->getPropertiesFile()) as $line) {
            if(!preg_match('/^#/', $line) && preg_match('/=/', $line)) {
                $exploded = explode('=', preg_replace('/\n?/', '', $line));
                $this->properties[$exploded[0]] = $exploded[1];
            }
        }

        return $this->properties;
    }

    /**
     * Get the properties file
     *
     * @return string
     */
    public function getPropertiesFile()
    {
        if(file_exists($this->propertiesFile)) {
            return $this->propertiesFile;
        }

        return base_path('public/downloads/basic_server.properties');
    }

    /**
     * Get a property
     *
     * @param string  $property
     * @return mixed
     */
    public function get($property)
    {
        return $this->properties[$property];
    }

    /**
     * Set a property
     *
     * @param string  $property
     * @param mixed  $value
     * @return mixed
     */
    public function set($property, $value)
    {
        $this->properties[$property] = $value;

        return $this->properties[$property];
    }

    /**
     * Get the server
     *
     * @return Server
     */
    public function getServer()
    {
        return $this->server;
    }

    /**
     * Save the properties to file
     */
    public function save()
    {
        $contents = [
            '# Generated with ' . config('app.name') . ' on ' . date('M d, Y h:i A', time()),
            '# Link: ' . config('app.link'),
        ];

        foreach($this->properties as $key => $property) {
            $contents[] = $key . '=' . $property;
        }

        $content = implode(PHP_EOL, $contents);

        file_put_contents($this->propertiesFile, $content);
    }

    public function __get($property)
    {
        return $this->$property;
    }
}