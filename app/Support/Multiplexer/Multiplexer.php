<?php

namespace App\Support\Multiplexer;


class Multiplexer
{
    /**
     * Connection to use
     *
     * @var string
     */
    protected $connection;

    /**
     * The multiplexer class
     *
     * @var AbstractMultiplexer
     */
    protected $class;

    /**
     * Multiplexer constructor.
     *
     * @param string  $connection
     */
    public function __construct($connection = null)
    {
        $connection = empty($connection) ? $this->getConfig('default') : $connection;
        $this->connection = $this->getConfig('drivers.' . $connection);
        $this->class = new $this->connection;
    }

    /**
     * Get config value
     *
     * @param string  $key
     * @return mixed
     */
    public function getConfig($key)
    {
        return config('multiplexer.' . $key);
    }

    /**
     * Call the driver class
     *
     * @param string  $name
     * @param mixed  $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return $this->class->{$name}(...$arguments);
    }
}