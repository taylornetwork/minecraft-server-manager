<?php

namespace App\Support\Multiplexer;


interface MultiplexerInterface
{
    /**
     * Get the multiplexer command to run for the specified action
     *
     * @param string  $action
     * @return string
     */
    public function getCommand($action);

    /**
     * Get the multiplexer command
     *
     * @return string
     */
    public function getBaseCommand();

    /**
     * Create a new session
     *
     * @param string  $name
     * @param string  $command
     * @return string
     */
    public function newSession($name, $command);


    /**
     * Attach to a named session
     *
     * @param string  $name
     * @return string
     */
    public function attachSession($name);

    /**
     * List all active sessions
     *
     * @return string
     */
    public function listSessions();

    /**
     * Kill a named session
     *
     * @param string  $name
     * @return string
     */
    public function killSession($name);

    /**
     * Check if a named session exists
     *
     * @param string  $name
     * @return string
     */
    public function hasSession($name);

    /**
     * Send a command to the multiplexer
     *
     * @param string  $name
     * @param string  $command
     * @return string
     */
    public function sendCommand($name, $command);

    /**
     * Capture the screen
     *
     * @param string  $name
     */
    public function captureScreen($name);

    /**
     * Grab the screen after capture
     *
     * @param string  $buffer
     * @return mixed
     */
    public function getScreen($buffer);
}