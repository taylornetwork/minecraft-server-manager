<?php

namespace App\Support\Multiplexer;


abstract class AbstractMultiplexer implements MultiplexerInterface
{
    /**
     * The base multiplexer command
     *
     * @var string
     */
    protected $baseCommand;

    /**
     * Map of commands for actions
     *
     * @var array
     */
    protected $commands;

    /**
     * @inheritdoc
     */
    public function getBaseCommand()
    {
        return $this->baseCommand;
    }

    /**
     * @inheritdoc
     */
    public function getCommand($action)
    {
        return $this->getBaseCommand() . ' ' . $this->commands[$action];
    }

    /**
     * Replace variables in a string
     *
     * @param string  $string
     * @param array  $variables
     * @return string
     */
    public function replace($string, $variables)
    {
        return preg_replace_callback('/{(.*?)}/', function ($m) use ($variables) {
            return $variables[$m[1]];
        }, $string);
    }
}