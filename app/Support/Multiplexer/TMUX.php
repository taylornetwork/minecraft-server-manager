<?php

namespace App\Support\Multiplexer;


final class TMUX extends AbstractMultiplexer
{
    protected $baseCommand = 'tmux';

    /**
     * TMUX constructor.
     */
    public function __construct()
    {
        $this->commands = config('multiplexer.commands.tmux');
    }

    /**
     * @inheritdoc
     */
    public function newSession($name, $command)
    {
        return $this->buildCommand('newSession', compact('name', 'command'));
    }

    /**
     * @inheritdoc
     */
    public function attachSession($name)
    {
        return $this->buildCommand('attachSession', compact('name'));
    }

    /**
     * @inheritdoc
     */
    public function listSessions()
    {
        return $this->buildCommand('listSessions');
    }

    /**
     * @inheritdoc
     */
    public function killSession($name)
    {
        return $this->buildCommand('killSession', compact('name'));
    }

    /**
     * @inheritdoc
     */
    public function hasSession($name)
    {
        return $this->buildCommand('hasSession', compact('name'));
    }

    /**
     * @inheritDoc
     */
    public function sendCommand($name, $command)
    {
        return $this->buildCommand('sendCommand', compact('name', 'command'));
    }

    /**
     * @inheritDoc
     */
    public function captureScreen($name, $lines = 100)
    {
        return $this->buildCommand('captureScreen', compact('name', 'lines'));
    }

    /**
     * @inheritDoc
     */
    public function getScreen($name, $lines = 100)
    {
        return $this->captureScreen($name, $lines);
    } 


    /**
     * Build the command
     *
     * @param string  $action
     * @param array  $compactedArguments
     * @return string
     */
    public function buildCommand($action, $compactedArguments = [])
    {
        return $this->replace($this->getCommand($action), $compactedArguments);
    }
}