<?php

namespace App\Support\Server\ModZipper;

use Carbon\Carbon;
use ZipArchive;
use App\Server;

class Zipper
{
    protected $filename;

    protected $pattern;

    protected $zipCreated = false;

    protected $server;

    public $force = false;

    public function __construct(Server $server)
    {
        $this->server = $server;
        $this->filename = $server->name . '_mods.zip';
        $this->pattern = config('zipper.file_pattern', '*.jar');

        if(!$this->isModdedServer()) {
            throw new \Exception('Server is not modded!');
        }

        if(!$this->checkForModsFolder()) {
            throw new \Exception('\'mods\' folder is missing.');
        }
    }

    public function getModList()
    {
        return glob($this->server->getPath('mods/' . $this->pattern));
    }

    public function checkForExistingArchive()
    {
        return file_exists($this->server->getPath($this->filename));
    }

    public function zip()
    {
        if(!$this->force) {
            if($this->checkForExistingArchive()) {
                $existing = new Carbon(date('Y-m-d H:i:s', filemtime($this->server->getPath($this->filename))));

                if(Carbon::now()->diffInHours($existing) <= config('zipper.max_zip_age', 2)) {
                    $this->zipCreated = true;
                    return true;
                }
            }
        }

        $archive = new ZipArchive();

        if ($archive->open($this->server->getPath($this->filename), ZipArchive::CREATE | ZipArchive::OVERWRITE)) {
            // loop through all the files and add them to the archive.
            foreach ($this->getModList() as $file) {
                if ($archive->addFile($file, basename($file))) {
                    // do something here if addFile succeeded, otherwise this statement is unnecessary and can be ignored.
                    continue;
                } else {
                    throw new Exception("file `{$file}` could not be added to the zip file: " . $archive->getStatusString());
                }
            }

            // close the archive.
            if ($archive->close()) {
                $this->zipCreated = true;
            } else {
                throw new Exception("could not close zip file: " . $archive->getStatusString());
            }
        } else {
            throw new Exception("zip file could not be created: " . $archive->getStatusString());
        }
    }

    public function getZipPath()
    {
        if(!$this->zipCreated) {
            $this->zip();
        }
        return $this->server->getPath($this->filename);
    }

    public function getZipDownloadURL()
    {
        if(!$this->zipCreated) {
            $this->zip();
        }
        return url('server/files/' . $this->server->name . '/' . $this->filename);
    }

    public function checkForModsFolder()
    {
        return file_exists($this->server->getPath('mods'));
    }

    public function isModdedServer()
    {
        return $this->server->modded;
    }

}