<?php

namespace App\Support\Server;

use App\Server;
use App\Support\Properties\Manager;
use App\Support\Server\Settler\AbstractSettler;
use App\Support\Shell\ShellCommand;

class Creator
{
    /**
     * Server
     *
     * @var Server
     */
    protected $server;

    /**
     * Show echoed messages
     *
     * @var bool
     */
    public $showMessages = true;

    /**
     * @var AbstractSettler
     */
    public $settler;

    /**
     * Creator constructor.
     *
     * @param Server $server
     */
    public function __construct(Server $server)
    {
        $this->server = $server;
    }

    /**
     * Do creation tasks.
     *
     * @return string
     */
    public function create()
    {
        $this->setUpDirectory();
        $this->acceptEULA();
        $this->saveInstaller();
        $this->performInstall();
        $this->setBasicServerProperties();
        $this->performCleanUp();
        $this->updateDatabase();

        return $this->getSettler()->getServerJar();
    }

    /**
     * Update database fields
     *
     * @return bool
     */
    public function updateDatabase()
    {
        $this->msg('Updating database...');
        return $this->server->update([
            'created' => true,
            'server_jar' => $this->getSettler()->getServerJar(),
            'modded' => $this->getSettler()->isServerModded(),
        ]);
    }

    /**
     * Clean up
     *
     * @return bool
     */
    public function performCleanUp()
    {
        $this->msg('Cleaning up...');
        return $this->getSettler()->cleanUp();
    }

    /**
     * Since server.properties wouldn't exist, grab a basic one and
     * override some of the values with the DB values
     *
     * @return array
     */
    public function setBasicServerProperties()
    {
        $this->msg('Setting basic server properties...');
        $manager = new Manager($this->server);

        foreach($this->server->propertiesMap as $dbKey => $propertiesKey) {
            if($this->server->$dbKey !== null) {
                $manager->set($propertiesKey, $this->server->$dbKey);
            }
        }

        $manager->save();

        return $manager->properties;
    }

    /**
     * Do all directory set up
     *
     * @return bool
     */
    public function setUpDirectory()
    {
        $this->msg('Creating directory \'' . $this->server->getPath() . '\'...');

        if(file_exists($this->server->getPath())) {
            return true;
        }

        return mkdir($this->server->getPath());
    }

    /**
     * Get the settler
     *
     * @return AbstractSettler
     */
    public function getSettler()
    {
        if(!isset($this->settler)) {
            $settlerClass = config('settler.' . $this->server->type . '.settler');
            $this->settler = new $settlerClass($this->server);
        }

        return $this->settler;
    }

    /**
     * Download and save the installer
     *
     * @return mixed
     */
    public function saveInstaller()
    {
        $this->msg('Downloading MineCraft Server...');
        return $this->getSettler()->saveInstaller();
    }

    /**
     * Install the server
     *
     * @return ShellCommand
     */
    public function performInstall()
    {
        $this->msg('Installing Server...');
        return $this->getSettler()->installServer();
    }

    /**
     * Accept EULA
     *
     * @return bool|int
     */
    public function acceptEULA()
    {
        $this->msg('Accepting EULA...');
        return $this->getSettler()->eula(true);
    }

    /**
     * Echo a message
     *
     * @param string  $msg
     * @param bool  $newLine
     */
    public function msg($msg, $newLine = true)
    {
        if($this->showMessages) {
            echo $msg . ($newLine ? PHP_EOL : '');
        }
    }
}