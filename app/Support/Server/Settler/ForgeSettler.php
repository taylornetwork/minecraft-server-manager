<?php

namespace App\Support\Server\Settler;

use App\Server;
use App\Support\Shell\ShellCommand;
use GuzzleHttp\Client;

final class ForgeSettler extends AbstractSettler
{
    /**
     * @var Client
     */
    protected $httpClient;

    /**
     * @inheritDoc
     */
    protected $driver = 'forge';

    /**
     * Name of the downloaded installer
     *
     * @var string
     */
    protected $installerName;

    /**
     * ForgeSettler constructor.
     *
     * @param Server  $server
     */
    public function __construct(Server $server)
    {
        parent::__construct($server);

        $this->httpClient = new Client();
    }

    /**
     * Get the installer URL
     *
     * @return string
     */
    public function getInstallerURL()
    {
        $htmlContent = $this->httpClient->get($this->getConfig('url'))->getBody()->getContents();
        $found = [];

        foreach(explode(PHP_EOL, $htmlContent) as $lineNumber => $line) {
            if(preg_match($this->getConfig('installerSearchPattern'), $line)) {
                $found[] = $line;
            }
        }

        $useLine = $this->getConfig('useRecommended', true) ? $found[1] : $found[0];

        preg_match('/&url=.*?"/', $useLine, $match);

        $url = substr(preg_replace('/^&url=/', '', $match[0]), 0, -1);

        $this->installerName = last(explode('/', $url));

        return $url;
    }

    /**
     * Get name of installer
     *
     * @return string
     */
    public function getInstallerName()
    {
        if(isset($this->installerName)) {
            return $this->installerName;
        }

        return last(explode(DIRECTORY_SEPARATOR , glob($this->getPath() . '*-installer.jar')[0]));
    }

    /**
     * @inheritDoc
     */
    public function getServerJar()
    {
        return last(explode(DIRECTORY_SEPARATOR, glob($this->getPath() . '*-universal.jar')[0]));
    }


    /**
     * @inheritDoc
     */
    public function saveInstaller()
    {
        return copy($this->getInstallerURL(), $this->getPath() . $this->getInstallerName());
    }

    /**
     * @inheritDoc
     */
    public function installServer()
    {
        chdir($this->getPath());
        return new ShellCommand('java -jar ' . $this->getInstallerName() . ' --installServer');
    }

    /**
     * @inheritDoc
     */
    public function cleanUp()
    {
        unlink($this->getPath() . $this->getInstallerName());
        unlink($this->getPath() . $this->getInstallerName() . '.log');

        return true;
    }

    /**
     * @inheritDoc
     */
    public function isServerModded()
    {
        return true;
    }
}