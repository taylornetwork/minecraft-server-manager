<?php

namespace App\Support\Server\Settler;


use App\Server;
use App\Support\Shell\ShellCommand;
use GuzzleHttp\Client;

final class VanillaSettler extends AbstractSettler
{
    /**
     * Driver
     *
     * @var string
     */
    protected $driver = 'vanilla';

    /**
     * Server JAR file
     *
     * @var string
     */
    protected $serverJar;

    /**
     * @var Client
     */
    protected $httpClient;

    /**
     * VanillaSettler constructor.
     *
     * @param Server $server
     */
    public function __construct(Server $server)
    {
        parent::__construct($server);

        $this->httpClient = new Client();
    }

    /**
     * Get the download URL
     *
     * @return string
     */
    public function getDownloadURL()
    {
        $url = $this->getConfig('url');

        $htmlContent = $this->httpClient->get($url)->getBody()->getContents();
        $found = [];

        foreach(explode(PHP_EOL, $htmlContent) as $lineNumber => $line) {
            if(preg_match($this->getConfig('downloadSearchPattern'), $line)) {
                $found[] = $line;
            }
        }

        preg_match('#href="(.*)"#', $found[0], $matches);

        $this->serverJar = last(explode('/', $matches[1]));

        return $matches[1];
    }


    /**
     * @inheritDoc
     */
    public function saveInstaller()
    {
        return copy($this->getDownloadURL(), $this->getPath() . $this->serverJar);
    }

    /**
     * @inheritDoc
     */
    public function installServer()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function cleanUp()
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function getServerJar()
    {
        if(isset($this->serverJar)) {
            return $this->serverJar;
        }

        $this->getDownloadURL();
        return $this->serverJar;
    }

    /**
     * @inheritDoc
     */
    public function isServerModded()
    {
        return false;
    }

}