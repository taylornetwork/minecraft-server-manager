<?php

namespace App\Support\Server\Settler;


use App\Server;
use App\Support\Shell\ShellCommand;
use GuzzleHttp\Client;

final class SpongeSettler extends AbstractSettler
{
    /**
     * Forge will be used with Sponge
     *
     * @var ForgeSettler
     */
    protected $forgeSettler;

    /**
     * @var Client
     */
    protected $httpClient;

    /**
     * Driver
     *
     * @var string
     */
    protected $driver = 'sponge';

    /**
     * SpongeSettler constructor.
     *
     * @param Server $server
     */
    public function __construct(Server $server)
    {
        parent::__construct($server);

        $this->forgeSettler = new ForgeSettler($server);

        $this->httpClient = new Client();
    }

    /**
     * Get Download URL
     *
     * @return string
     */
    public function getDownloadURL()
    {
        $url = $this->getConfig('baseUrl') . $this->getConfig('url');

        $htmlContent = $this->httpClient->get($url)->getBody()->getContents();
        $found = [];

        foreach(explode(PHP_EOL, $htmlContent) as $lineNumber => $line) {
            if(preg_match($this->getConfig('downloadSearchPattern'), $line)) {
                $found[] = $line;
            }
        }

        preg_match('#href="(.*)" #', $found[0], $matches);

        return $this->getConfig('baseUrl') . $matches[1];
    }

    /**
     * @inheritDoc
     */
    public function saveInstaller()
    {
        return $this->forgeSettler->saveInstaller();
    }

    /**
     * @inheritDoc
     */
    public function installServer()
    {
        $this->forgeSettler->installServer();

        $url = $this->getDownloadURL();
        $downloadAs = last(explode('/', $url));
        mkdir($this->getPath() . 'mods');
        return copy($url, $this->getPath() . 'mods' . DIRECTORY_SEPARATOR . $downloadAs);
    }

    /**
     * @inheritDoc
     */
    public function cleanUp()
    {
        $this->forgeSettler->cleanUp();

        return true;
    }

    /**
     * @inheritDoc
     */
    public function getServerJar()
    {
        return $this->forgeSettler->getServerJar();
    }

    /**
     * @inheritDoc
     */
    public function isServerModded()
    {
        return true;
    }

}