<?php

namespace App\Support\Server\Settler;


use App\Server;

abstract class AbstractSettler implements SettlerInterface
{
    /**
     * @var Server
     */
    protected $server;

    /**
     * Driver that is being used (MUST be overridden)
     *
     * @var string
     */
    protected $driver = null;

    /**
     * AbstractSettler constructor.
     *
     * @param Server  $server
     */
    public function __construct(Server $server)
    {
        $this->server = $server;
    }

    /**
     * Append a trailing slash if necessary
     *
     * @return string
     */
    public function getPath()
    {
        if(preg_match('#' . DIRECTORY_SEPARATOR . '$#', $this->server->getPath())) {
            return $this->server->getPath();
        }

        return $this->server->getPath() . DIRECTORY_SEPARATOR;
    }

    /**
     * @inheritDoc
     */
    public function eula($accepted)
    {
        return file_put_contents($this->getPath() . 'eula.txt', 'eula=' . ($accepted ? 'true' : 'false'));
    }

    /**
     * Load from config
     *
     * @param string  $key
     * @param null|string  $default
     * @return string
     */
    public function getConfig($key, $default = null)
    {
        return config(implode('.', [ 'settler', $this->driver, $key ]), $default);
    }
}