<?php

namespace App\Support\Server\Settler;

use App\Support\Shell\ShellCommand;

interface SettlerInterface
{
    /**
     * Download installer to server path
     *
     * @return mixed
     */
    public function saveInstaller();

    /**
     * Install the server
     *
     * @return ShellCommand
     */
    public function installServer();

    /**
     * Set the EULA terms
     *
     * @param string $accepted
     * @return int|bool
     */
    public function eula($accepted);

    /**
     * Clean up after install
     *
     * @return bool
     */
    public function cleanUp();

    /**
     * Get the server JAR file name
     *
     * @return string
     */
    public function getServerJar();

    /**
     * Is the server able to be modded?
     *
     * @return bool
     */
    public function isServerModded();
}