<?php

namespace App\Support\Server;

use App\Server;
use App\Support\Multiplexer\Multiplexer;
use App\Support\Shell\ShellCommand;

class Manager
{
    /**
     * @var Multiplexer
     */
    protected $multiplexer;

    /**
     * @var Server
     */
    protected $server;

    /**
     * Status names
     *
     * @var array
     */
    protected $statuses;

    /**
     * The screen was captured.
     *
     * @var bool
     */
    protected $screenCaptured = false;

    /**
     * ServerManager constructor.
     *
     * @param Server  $server
     */
    public function __construct(Server $server)
    {
        $this->multiplexer = new Multiplexer();
        $this->server = $server;

        $this->statuses = config('servermanager.statuses');
    }

    /**
     * Start the server if not started
     *
     * @return ShellCommand|bool
     */
    public function start()
    {
        if(!$this->isOn()) {
            return $this->forceStart();
        }
        return false;
    }

    /**
     * Safely stop the server
     *
     * @return ShellCommand|bool
     */
    public function stop()
    {
        return $this->sendCommand('stop');
    }

    /**
     * Force the start of the server
     *
     * @return ShellCommand
     */
    public function forceStart()
    {
        $startCommand = 'cd ' . $this->server->getPath() . ';';

        $startCommand .= $this->server->command;

        return new ShellCommand($this->multiplexer->newSession($this->server->name, $startCommand));
    }

    /**
     * Kill the server
     *
     * @return ShellCommand
     */
    public function forceStop()
    {
        return new ShellCommand($this->multiplexer->killSession($this->server->name));
    }

    /**
     * Send a command to the server
     *
     * @param string  $command
     * @return ShellCommand|bool
     */
    public function sendCommand($command)
    {
        if($this->isOn()) {
            return new ShellCommand($this->multiplexer->sendCommand($this->server->name, $command));
        }
        return false;
    }

    /**
     * Send a command and get the relevant output
     *
     * @param string  $command
     * @return array
     */
    public function send($command)
    {
        $this->sendCommand($command);
        sleep(1);
        $screen = $this->getScreen();

        $output = $screen->output;
        $counter = count($output) - 2; // -2: arrays start at 0 and we don't need the last '>'

        $reversedOutput = [];

        while($counter >= 0) {
            $reversedOutput[] = $output[$counter];

            if($output[$counter] === '> ' . $command) {
                break;
            }

            --$counter;
        }


        return array_reverse($reversedOutput);
    }

    /**
     * Capture the screen
     *
     * @return ShellCommand
     */
    public function captureScreen()
    {
        $shell = new ShellCommand($this->multiplexer->captureScreen($this->server->name));
        $this->screenCaptured = true;
        return $shell;
    }

    /**
     * Get the captured screen
     *
     * @param int lines
     * @return ShellCommand
     */
    public function getScreen($lines = 100)
    {
        return new ShellCommand($this->multiplexer->captureScreen($this->server->name, $lines));
    }

    /**
     * Check if the server is on
     *
     * @return bool
     */
    public function isOn()
    {
        return $this->status() === $this->statuses['on'];
    }

    /**
     * Get the status of the server
     *
     * @return string
     */
    public function status()
    {
        $shell = new ShellCommand($this->multiplexer->hasSession($this->server->name));

        if($shell->exitCode === 0) {
            return $this->statuses['on'];
        } else {
            return $this->statuses['off'];
        }
    }
}