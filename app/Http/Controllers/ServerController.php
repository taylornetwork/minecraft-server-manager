<?php

namespace App\Http\Controllers;

use App\Server;
use App\Support\Server\ModZipper\Zipper;
use Illuminate\Http\Request;

class ServerController extends Controller
{
    public function all()
    {
        $servers = Server::all();
        return view('server.all', compact('servers'));
    }

    public function properties(Server $server)
    {
        $properties = $server->getProperties();

        return view('server.properties', compact('server', 'properties'));
    }

    public function edit(Server $server)
    {
        return view('server.edit', compact('server'));
    }

    public function create()
    {
        return view('server.create');
    }
}
