<?php

namespace App\Http\Controllers\API;

use App\Server;
use App\Support\Properties\Manager;
use App\Support\Server\ModZipper\Zipper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServerController extends Controller
{
    public function getStatus(Server $server)
    {
        $sinceStart = $server->started_at === null ? 100 : $server->started_at->diffInMinutes(Carbon::now());
        $starting = ($server->isOn() ? !$server->doneStarting() : false);

        return [
            'status' => $server->status(),
            'started_at' => $server->started_at,
            'starting' => ($sinceStart < 60 ? $starting : false),
        ];
    }

    public function waitForStart(Server $server)
    {
        $server->waitForStart();
    }

    public function startServer(Server $server)
    {
        $start = $server->start();

        $timer = 0;

        while(!$server->isOn()) {

            if($timer >= 240 || $start->exitCode !== 0) {
                break;
            }

            sleep(0.25);
            $timer += 1;
        }

        if($start->exitCode === 0) {
            return 'Started.';
        }
        return 'Failed.';
    }

    public function stopServer(Server $server)
    {
        $stop = $server->stop();

        $timer = 0;

        while($server->isOn()) {

            if($timer >= 60 || $stop->exitCode !== 0) {
                break;
            }

            sleep(1);
            $timer += 1;
        }

        if($stop->exitCode === 0) {
            return 'Stopped.';
        }

        return 'Failed.';
    }

    public function killServer(Server $server)
    {
        $kill = $server->kill();

        $timer = 0;

        while($server->isOn()) {

            if($timer >= 60 || $kill->exitCode !== 0) {
                break;
            }

            sleep(1);
            $timer += 1;
        }

        if($kill->exitCode === 0) {
            return 'Killed.';
        }
        return 'Failed.';
    }

    public function sendCommand(Server $server, Request $request)
    {
        return $server->send($request->command);
    }

    public function players(Server $server)
    {
        return $server->players();
    }

    public function store(Request $request)
    {
        return Server::create($request->all());
    }

    public function destroy(Server $server)
    {
        $server->delete();
        return response('OK', 200);
    }

    public function update(Server $server, Request $request)
    {
        $data = $request->all();

        if(array_key_exists('properties', $data)) {
            $manager = new Manager($server);

            foreach($server->propertiesMap as $db => $file) {
                if($data['properties'][$file]) {
                    $data['server'][$db] = $data['properties'][$file];
                }
            }

            foreach($data['properties'] as $property => $value) {
                $manager->set($property, $value);
            }

            $manager->save();
        }

        $server->update(array_only($data['server'], $server->getFillable()));

        return response('OK', 200);
    }

    public function zipMods(Server $server)
    {
        $zipper = new Zipper($server);
        return response($zipper->getZipDownloadURL(), 200);
    }
}

