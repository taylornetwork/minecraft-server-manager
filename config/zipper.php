<?php

return [
    'file_pattern' => env('ZIPPER_FILE_PATTERN', '*.jar'),

    'max_zip_age' => env('ZIPPER_MAX_AGE', 2),
];