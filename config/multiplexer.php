<?php

return [

    'default' => 'tmux',

    'drivers' => [
        'tmux' => \App\Support\Multiplexer\TMUX::class,
    ],

    'commands' => [
        'tmux' => [
            'newSession' => 'new-session -d -s {name} "{command}"',
            'listSessions' => 'list-sessions',
            'killSession' => 'kill-session -t {name}',
            'attachSession' => 'attach -t {name}',
            'hasSession' => 'has-session -t {name}',
            'sendCommand' => 'send-keys -t {name} "{command}" Enter',
            'captureScreen' => 'capture-pane -t {name} -pS -{lines}',
            'getScreen' => 'show-buffer -b {buffer}',
        ],
    ],

];