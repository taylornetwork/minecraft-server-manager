<?php

return [
    'statuses' => [
        'on' => 'Running',
        'off' => 'Not Running',
    ],
];