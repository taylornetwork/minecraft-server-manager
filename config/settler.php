<?php

return [

    'forge' => [

        'settler' => \App\Support\Server\Settler\ForgeSettler::class,

        'url' => 'https://files.minecraftforge.net',

        'useRecommended' => true,

        'installerSearchPattern' => '/title="Installer"/',

    ],

    'vanilla' => [
        'settler' => \App\Support\Server\Settler\VanillaSettler::class,

        'url' => 'https://minecraft.net/en-us/download/server',

        'downloadSearchPattern' => '/minecraft_server/',
    ],

    'sponge' => [
        'settler' => \App\Support\Server\Settler\SpongeSettler::class,

        'url' => '/maven/org/spongepowered/spongeforge',

        'baseUrl' => 'http://files.minecraftforge.net',

        'downloadSearchPattern' => '/title="Mod"/',
    ],

];