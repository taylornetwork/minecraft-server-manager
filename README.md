# Minecraft Server Manager

Web application for the management of Minecraft servers. 

## Server Requirements

- Ubuntu 16.04 or above (untested below 16.04 so could work)
- [All Laravel Server Requirements](https://laravel.com/docs/master#server-requirements)

## Install

I have written a [Provision Script](https://gitlab.com/snippets/1813582) which will handle the heavy lifting for you.

Download using wget

```bash
$ wget -qO mcinit.sh https://gitlab.com/snippets/1813582/raw
```

You will need to modify the bash variables in the downloaded file to your desired settings before running.

*Note: the `provision` variable should be set to false if you have already installed all the necessary Laravel dependencies, it should be true when being run on a clean machine.*

When ready run

```bash
$ bash mcinit.sh
```

