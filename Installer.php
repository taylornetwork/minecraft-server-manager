<?php

require implode(DIRECTORY_SEPARATOR, [ 'app', 'Support', 'Shell', 'ShellCommand.php' ]);

use App\Support\Shell\ShellCommand;

class Installer
{
    /**
     * Development Mode
     *
     * Set to true to build assets in dev mode
     *
     * @var bool
     */
    public $devMode = false;

    /**
     * Echo messages
     *
     * @var bool
     */
    public $displayMessages = true;

    /**
     * If we created a .env file
     *
     * @var bool
     */
    protected $copiedEnv = false;

    /**
     * Check to see if the vendor folder exists
     *
     * If it exists, we run an update, otherwise its a fresh install
     *
     * @return bool
     */
    public function checkForVendorFolder()
    {
        $exists = file_exists('vendor');

        $this->msg('?> \'vendor\' folder exists: ' . ($exists ? 'true' : 'false'));

        return $exists;
    }

    /**
     * Check if the package-lock file exists
     *
     * If it does, update otherwise, install
     *
     * @return bool
     */
    public function checkForPackageLockFile()
    {
        $exists = file_exists('package-lock.json');

        $this->msg('?> \'package-lock.json\' exists: ' . ($exists ? 'true' : 'false'));

        return $exists;
    }

    /**
     * Handle the composer dependencies
     *
     * @return ShellCommand|string
     */
    public function handleComposerDeps()
    {
        if(!$this->checkForVendorFolder()) {
            $this->msg('$ composer install');
            return exec('composer install');
        }

        $this->msg('$ composer update');
        return new ShellCommand('composer update');
    }

    /**
     * Handle the NPM dependencies
     *
     * @return ShellCommand
     */
    public function handleNPMDeps()
    {
        $command = 'npm ' . ($this->checkForPackageLockFile() ? 'update' : 'install');

        $this->msg('$ ' . $command);

        return new ShellCommand($command);
    }

    /**
     * Build the NPM assets
     *
     * @return ShellCommand
     */
    public function buildAssets()
    {
        $command = 'npm run ' . ($this->devMode ? 'dev' : 'production');

        $this->msg('$ ' . $command);

        return new ShellCommand($command);
    }

    /**
     * Create a .env file if one doesn't exist
     */
    public function checkForEnvFile()
    {
        if(count(glob('.env')) === 0) {

            $this->msg('$ copy .env.example -> .env');

            copy('.env.example', '.env');
            $this->copiedEnv = true;
        }
    }

    /**
     * Generate a key for Laravel
     *
     * @return ShellCommand
     */
    public function generateKey()
    {
        $command = 'php artisan key:generate';
        $this->msg('$ ' . $command);
        return new ShellCommand($command);
    }

    /**
     * Migrate Database
     *
     * @return ShellCommand
     */
    public function migrate()
    {
        $command = 'php artisan migrate';

        $this->msg('$ ' . $command);

        return new ShellCommand($command);
    }

    /**
     * Set maintenance mode
     *
     * @param bool  $on
     * @return ShellCommand
     */
    public function setMaintenanceMode($on)
    {
        $command = 'php artisan ' . ($on ? 'down' : 'up');

        $this->msg('$ ' . $command);

        return new ShellCommand($command);
    }

    /**
     * Do the install
     *
     * @return bool
     */
    public function doFullInstall()
    {
        if($this->checkForVendorFolder()) {
            $this->msg('Putting application in maintenance mode...');
            $this->setMaintenanceMode(true);
            $this->msg('Done.');
            $this->space();
        }

        $this->msg('Handle Composer Dependencies...');
        $this->handleComposerDeps();
        $this->msg('Done.');
        $this->space();

        $this->msg('Check for existing .env file...');
        $this->checkForEnvFile();
        $this->msg('Done.');
        $this->space();

        if($this->copiedEnv) {
            $this->generateKey();
            $this->msg('Created .env file and generated a key for you, please edit that file to add database info and run this again.');
            return false;
        }

        $this->msg('Handle NPM Dependencies...');
        $this->handleNPMDeps();
        $this->msg('Done.');
        $this->space();

        $this->msg('Build NPM Assets...');
        $this->buildAssets();
        $this->msg('Done.');
        $this->space();

        $this->msg('Migrate Database...');
        $this->migrate();
        $this->msg('Done.');
        $this->space();


        $this->msg('Taking application out of maintenance mode...');
        $this->setMaintenanceMode(false);
        $this->msg('Done.');
        $this->space();
        return true;
    }

    /**
     * Basically echo if enabled
     *
     * @param string  $message
     * @param bool  $eol
     */
    public function msg($message, $eol = true)
    {
        if($this->displayMessages) {
            echo $message . ($eol ? PHP_EOL : '');
        }
    }

    /**
     * Echo blank space
     */
    public function space()
    {
        $this->msg(null);
    }
}