@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @foreach($properties as $key => $value)
                    <property name="{{ $key }}" value="{{ $value }}"></property>
                @endforeach
            </div>
        </div>
    </div>
@endsection
