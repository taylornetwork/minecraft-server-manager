@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                @if(!auth()->guest())
                    <a href="{{ route('server.create') }}" class="btn btn-success">
                        <i class="fa fa-fw fa-plus"></i>
                        New Server
                    </a>
                    <hr>
                @endif

                @foreach($servers as $server)
                    <server :server="{{ $server }}" {{ !auth()->guest() ? 'show-options' : '' }}></server>
                @endforeach
            </div>
        </div>
    </div>
@endsection
